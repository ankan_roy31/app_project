<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['category_list'] = Category::where('created_by', Auth::id())->get();
        return view('categories.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $category = new Category();
        $category->name = $request->category_name;
        $category->created_by = Auth::id();
        $category->save();

        return redirect('/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($category_id)
    {
        $category = Category::where('created_by', Auth::id())->find($category_id);
        if (!$category) {
            return redirect('/categories');
        }
        $data['category'] = $category;
        return view('categories.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category_id)
    {
        $category = Category::where('created_by', Auth::id())->find($category_id);
        if (!$category) {
            return redirect('/categories');
        }
        $category->name = $request->category_name;
        $category->save();
        return redirect('/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($category_id)
    {
        $category = Category::where('created_by', Auth::id())->find($category_id);
        if (!$category) {
            return redirect('/categories');
        }
        $category->delete();
        return redirect('/categories');
    }

    public function viewtask($category_id)
    {
        $category = Category::where('created_by', Auth::id())->find($category_id);
        if (!$category) {
            return redirect('/categories');
        }
        $tasks = Task::where('category_id', $category_id)->get();
        $data['tasks'] = $tasks;
        return view('categories.viewTasks', $data);
    }
}
