<?php

namespace App\Http\Controllers;

use App\Enums\Tasksstatus;
use App\Http\Requests\TaskRequest;
use App\Models\Category;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['tasks']= Task::where('created_by', Auth::id())->get();
        return view('tasks.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["task_status"]= Tasksstatus::asSelectArray();
        $data["category_list"]= Category::where('created_by', Auth::id())->get();

        return view('tasks.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskRequest $request)
    {
        $task = new task();
        $task->name = $request-> task_name;
        $task->category_id = $request-> category_id;
        $task->details = $request-> task_details;
        $task->deadline = $request-> task_deadline;
        $task->status = $request-> task_status;
        $task->created_by = Auth::id();
        $task->save();
        return redirect('/tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($task_id)
    {
        $data["task_status"]= Tasksstatus::asSelectArray();
        $data["category_list"]= Category::where('created_by', Auth::id())->get();
        $task = Task::where('created_by', Auth::id())->find($task_id);
        if (!$task) {
            return redirect('/tasks');
        }
        $data['task'] = $task;
        return view('tasks.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $task_id)
    {
        $task = Task::where('created_by', Auth::id())->find($task_id);
        if (!$task) {
            return redirect('/tasks');
        }         

        $task->name = $request-> task_name;
        $task->category_id = $request-> category_id;
        $task->details = $request-> task_details;
        $task->deadline = $request-> task_deadline;
        $task->status = $request-> task_status;
        $task->created_by = Auth::id();
        $task->save();
        return redirect('/tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($task_id)
    {
        $task = Task::where('created_by', Auth::id())->find($task_id);
        if (!$task) {
            return redirect('/tasks');
        }
        $task->delete();
        return redirect('/tasks');
    }
}
