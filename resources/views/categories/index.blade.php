@extends('layouts.app')
@section('contents')
<a href="{{ url('/categories/create') }}" class="btn btn-success">Add New Category</a>
<hr>

<table class="table table-bordered">
    <tr>
        <th>Name</th>
        <th>Action</th>
        <th>View All Tasks</th>
    </tr>
    @foreach ($category_list as $category)
    <tr>
        <td>{{ $category->name }}</td>
        <td>
            <a href="{{ url("/categories/$category->id/edit")}}" class="btn btn-warning btn-sm">Update</a>
            <form action="{{ url("/categories/$category->id")}}" method="post" style="display: inline-block;"
                onsubmit="return confirm('Do you really want to delete this category?');">
                @method('delete')
                @csrf
                <button type="submit" class="btn btn-danger btn-sm">delete</button>
            </form>
        </td>
        <td>
            <a href="{{ url("/categories/$category->id/viewtask")}}" class="btn btn-primary btn-sm">View</a>
        </td>
    </tr>
    @endforeach
</table>
@endsection