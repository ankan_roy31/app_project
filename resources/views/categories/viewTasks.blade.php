@extends('layouts.app')
@section('contents')

<table class="table table-bordered">
    <tr>
        <th>Name</th>
        <th>Details</th>
        <th>Deadline</th>
        <th>Status</th>
    </tr>
    @foreach ($tasks as $task)
    <tr>
        <td>{{ $task->name }}</td>
        <td>{{ $task->details }}</td>
        <td>{{ $task->deadline }}</td>
        <td>{{ App\Enums\Tasksstatus::getDescription($task->status) }}</td>
    </tr>
    @endforeach
</table>

@endsection