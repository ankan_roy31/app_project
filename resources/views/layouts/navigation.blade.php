<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">WebSiteName</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="{{url('/dashboard')}}">Dashboard</a></li>
            <li class="active"><a href="{{ url('/tasks') }}">Tasks</a></li>
            <li class="active"><a href="{{ url('/categories') }}">Categories</a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li>
                <form action="{{ route('logout') }}" method="post">
                  @csrf
                    <button type="submit" class="btn btn-link" style="padding-top: 15px;"><span class="glyphicon glyphicon-log-out">Logout</span></button>
                </form>
            </li>
        </ul>

    </div>
</nav>
