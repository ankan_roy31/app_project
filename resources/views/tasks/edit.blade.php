@extends('layouts.app')
@section('contents')
<h3>Update Task</h3>
<form class="form-horizontal" action="{{ url("/tasks/{$task->id}") }}" method="POST">
    @csrf
    <div class="form-group">
        <label class="control-label col-sm-2" for="task_name">Task Name:</label>
        <div class="col-sm-10">
            <input type="text" name="task_name" class="form-control" id="task_name"
                value="{{ $task->name }}" placeholder="Task Name">
        </div>
    </div>
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="task_details">Task Details:</label>
        <div class="col-sm-10">
            <textarea name="task_details" id="task_details" cols="30" rows="10" class="form-control">{{ $task->details }}</textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="task_deadline">Task Deadline:</label>
        <div class="col-sm-3">
            <input type="date" name="task_deadline" class="form-control" id="task_deadline"
                value="{{ $task->deadline }}">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="task_status">Task Status:</label>
        <div class="col-sm-3">
            <select name="task_status" id="task_status" class="form-control">
                <option value="">---Select a Status---</option>
                @foreach ($task_status as $v => $status)
                <option value="{{ $v }}" {{ $task->status == $v ? 'selected' : ''}}>{{ $status }}</option>
                @endforeach
            </select>
        </div>
    </div>
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="category_id">Categories:</label>
        <div class="col-sm-3">
            <select name="category_id" id="category_id" class="form-control">
                <option value="">---Select a Category---</option>
                @foreach ($category_list as $v => $category)
                <option value="{{ $category->id }}" {{ $task->category_id == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Create</button>
        </div>
    </div>
</form>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@endsection