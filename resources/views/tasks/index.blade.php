@extends('layouts.app')

@section('contents')
<a href="{{ url('/tasks/create') }}" class="btn btn-success">Add New Task</a>
<hr>

<table class="table table-bordered">
    <tr>
        <th>Name</th>
        <th>Details</th>
        <th>Category</th>
        <th>Deadline</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
    @foreach ($tasks as $task)
    <tr>
        <td>{{ $task->name }}</td>
        <td>{{ $task->details }}</td>
        <td>{{ $task->category->name }}</td>
        <td>{{ $task->deadline }}</td>
        <td>{{ App\Enums\Tasksstatus::getDescription($task->status) }}</td>
        <td>
            <a href="{{ url("/tasks/$task->id/edit")}}" class="btn btn-warning btn-sm">Update</a>
            <form action="{{ url("/tasks/$task->id")}}" method="post" style="display: inline-block;"
                onsubmit="return confirm('Do you really want to delete this task?');">
                @method('delete')
                @csrf
                <button type="submit" class="btn btn-danger btn-sm">delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>
    
@endsection